#!/usr/bin/env python
# -*- coding: utf-8 -*-

from PyQt5.QtCore import Qt,QDate,QDateTime,QTime
from PyQt5.QtWidgets import QWidget,QListWidgetItem,QMessageBox
from forms.maintenceForm import Ui_MaintenceForm

import dbUtil
from commons import *
from tables import *

class MaintenceRecordForm(QWidget,Ui_MaintenceForm):


    def __init__(self):
        super(MaintenceRecordForm, self).__init__()

        # Set up the user interface from Designer.
        self.setupUi(self)
        self.init()

    def init(self):
        self.cmbJobCls.setModel(dbUtil.getJobClsModel())
        self.cmbJobCls.setModelColumn(1)
        self.cmbJober.setModel(dbUtil.getRepairerModel())
        self.cmbJober.setModelColumn(1)

        self.btnAdd.clicked.connect(self.onAddMaintence)
        self.btnDel.clicked.connect(self.onDelMaintence)
        self.btnOk.clicked.connect(self.onOk)
        self.btnCancel.clicked.connect(self.onCancel)
        self.mrModel = dbUtil.getMaintenceModel()
        for i in range(0,self.mrModel.rowCount()):
            di = self.mrModel.data(self.mrModel.index(i,TC_M_DATE))
            d = QDateTime.fromMSecsSinceEpoch(di)
            li = QListWidgetItem(d.date().toString(Qt.ISODate),self.lwDates)
            li.setData(DrEditTag,TagInit)
        self.lwDates.currentRowChanged.connect(self.onMaintenceSelectChanged)
        self.curIndex=-1  #当前选择的维护记录索引
        self.onMaintenceSelectChanged(-1)

    def inspectContent(self):
        changed = False
        dc = QDateTime(self.edtDate.date(),QTime()).toMSecsSinceEpoch()
        do = self.mrModel.data(self.mrModel.index(self.curIndex,TC_M_DATE))
        if(dc != do):
            self.mrModel.setData(self.mrModel.index(self.curIndex,TC_M_DATE),dc)
            changed=True
        mo = self.mrModel.data(self.mrModel.index(self.curIndex,TC_M_MILEGAE))
        mc = 0
        if(self.edtMileage.text()):
            mc = int(self.edtMileage.text())
        if(mo != mc):
            self.mrModel.setData(self.mrModel.index(self.curIndex,TC_M_MILEGAE),mc)
            changed = True
        m = self.cmbJobCls.model()
        jc_c = m.data(m.index(self.cmbJobCls.currentIndex(),0))
        jc_o = self.mrModel.data(self.mrModel.index(self.curIndex,TC_M_JOBCLS))
        if(jc_c != jc_o):
            self.mrModel.setData(self.mrModel.index(self.curIndex,TC_M_JOBCLS),jc_c)
            changed=True
        cost_o = self.mrModel.data(self.mrModel.index(self.curIndex,TC_M_TCOST))
        cost_c = 0.0
        if(self.edtTCost.text()):
            cost_c = float(self.edtTCost.text())
        if(cost_c != cost_o):
            self.mrModel.setData(self.mrModel.index(self.curIndex,TC_M_TCOST),cost_c)
            changed=True
        cost_o = self.mrModel.data(self.mrModel.index(self.curIndex,TC_M_MCOST))
        cost_c = 0.0
        if(self.edtMCost.text()):
            cost_c = float(self.edtMCost.text())
        if(cost_c != cost_o):
            self.mrModel.setData(self.mrModel.index(self.curIndex,TC_M_MCOST),cost_c)
            changed=True
        m=self.cmbJober.model()
        jer_c = m.data(m.index(self.cmbJober.currentIndex(),0))
        jer_o = self.mrModel.data(self.mrModel.index(self.curIndex,TC_M_REPAIRER))
        if(jer_c != jer_o):
            self.mrModel.setData(self.mrModel.index(self.curIndex,TC_M_REPAIRER),jer_c)
            changed = True
        cc = self.jobContents.toPlainText()
        co = self.mrModel.data(self.mrModel.index(self.curIndex,TC_M_CONTENTS))
        if(cc != co):
            self.mrModel.setData(self.mrModel.index(self.curIndex,TC_M_CONTENTS),cc)
            changed=True
        if(changed):
            li = self.lwDates.item(self.curIndex)
            tag = li.data(DrEditTag)
            if(tag == TagInit):
                li.setData(DrEditTag,TagChanged)


    def onMaintenceSelectChanged(self,row):
        if(self.curIndex != -1):
            self.inspectContent()
        self.curIndex = row
        if(row == -1):
            self.edtDate.setDate(QDate.currentDate())
            self.edtTCost.clear()
            self.edtMCost.clear()
            self.edtMileage.clear()
            self.cmbJobCls.setCurrentIndex(-1)
            self.cmbJober.setCurrentIndex(-1)
            self.jobContents.clear()
        else:
            dt = QDateTime.fromMSecsSinceEpoch(self.mrModel.data(self.mrModel.index(row,TC_M_DATE)))
            self.edtDate.setDate(dt.date())
            self.edtMCost.setText(str(self.mrModel.data(self.mrModel.index(row,TC_M_MCOST))))
            self.edtTCost.setText(str(self.mrModel.data(self.mrModel.index(row,TC_M_TCOST))))
            self.edtMileage.setText(str(self.mrModel.data(self.mrModel.index(row,TC_M_MILEGAE))))
            self.jobContents.setText(self.mrModel.data(self.mrModel.index(row,TC_M_CONTENTS)))
            jober = self.mrModel.data(self.mrModel.index(row,TC_M_REPAIRER))
            jc = self.mrModel.data(self.mrModel.index(row,TC_M_JOBCLS))
            fonded=False
            m=self.cmbJober.model()
            for i in range(0,m.rowCount()):
                id = m.data(m.index(i,0))
                if(id == jober):
                    self.cmbJober.setCurrentIndex(i)
                    fonded=True
                    break
            if(not fonded):
                self.cmbJober.setCurrentIndex(-1)
            fonded=False
            m=self.cmbJobCls.model()
            for i in range(0,m.rowCount()):
                id = m.data(m.index(i,0))
                if(id == jc):
                    self.cmbJobCls.setCurrentIndex(i)
                    fonded=True
                    break
            if(not fonded):
                self.cmbJobCls.setCurrentIndex(-1)

    def onAddMaintence(self):
        row = self.mrModel.rowCount()
        self.mrModel.insertRow(row)
        dt = QDateTime.currentDateTime()
        self.mrModel.setData(self.mrModel.index(row,TC_M_DATE),dt.toMSecsSinceEpoch())
        li = QListWidgetItem(dt.date().toString(Qt.ISODate),self.lwDates)
        li.setData(DrEditTag,TagNew)
        self.lwDates.setCurrentRow(row)

    def onDelMaintence(self):
        if(self.lwDates.currentRow() == -1):
            return
        li = self.lwDates.currentItem()
        li.setData(DrEditTag,TagDel)
        li.setHidden(True)
        self.lwDates.setCurrentRow(-1)

    def onOk(self):
        self.inspectContent()
        for i in range(self.lwDates.count()-1,-1,-1):
            tag = self.lwDates.item(i).data(DrEditTag)
            if(tag == TagDel):
                self.mrModel.removeRow(i)
                self.lwDates.takeItem(i)
        self.mrModel.submitAll()

        if(self.cmbJober.model().isDirty()):
            self.cmbJober.model().submitAll()
        if(self.cmbJobCls.model().isDirty()):
            self.cmbJobCls.model().submitAll()

    def onCancel(self):
        if(self.mrModel.isDirty()):
            self.mrModel.revertAll()
        for i in range(0,self.lwDates.count()):
            li = self.lwDates.item(i)
            if(li.data(DrEditTag) == TagDel):
                li.setData(DrEditTag,TagInit)
                li.setHidden(False)
        if(self.cmbJober.model().isDirty()):
            self.cmbJober.model().revertAll()
        if(self.cmbJobCls.model().isDirty()):
            self.cmbJobCls.model().revertAll()