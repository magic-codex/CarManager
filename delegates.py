#!/usr/bin/env python
# -*- coding: utf-8 -*-

from PyQt5.QtWidgets import QItemDelegate,QDateEdit
from PyQt5.QtCore import Qt,QDateTime
#from PyQt5.QtSql import QSqlRelationalTableModel

class DateDelegate(QItemDelegate):
    def __init__(self,column):
        super(DateDelegate, self).__init__()
        self.column=column

    def createEditor(self,parent, option, index):
        if(index.column() != self.column):
            return None
        editor = QDateEdit(parent)
        editor.setDisplayFormat("yyyy-MM-dd")
        return editor

    def setEditorData(self,editor, index):
        ti = index.model().data(index,Qt.EditRole)
        t = QDateTime.fromMSecsSinceEpoch(ti)
        editor.setDate(t.date())

    def setModelData(self,editor, model, index):
        ti = QDateTime(editor.date()).toMSecsSinceEpoch()
        model.setData(index,ti,Qt.EditRole)
        #model.setData(index,editor.date().toString(),Qt.DisplayRole)

