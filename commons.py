#!/usr/bin/env python
# -*- coding: utf-8 -*-

from PyQt5.QtCore import Qt

DrId,DrEditTag,DrContent,DrContent2 = range(Qt.UserRole,Qt.UserRole+4)  #数据角色值
TagInit,TagChanged,TagNew,TagDel = range(0,4)                           #修改标记 0：初始项，1：修改项，2：新增项,3删除项
FcClassCodeRole,FcFuelIdRole = range(Qt.UserRole,Qt.UserRole+2)         #燃料种类页的燃料级别表的第一列放置的燃料种类代码和对应燃料ID的枚举角色

