# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mainwindow.ui'
#
# Created: Fri Jan 22 09:20:06 2016
#      by: PyQt5 UI code generator 5.2.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 600)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.centralwidget)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.lwFaces = QtWidgets.QListWidget(self.centralwidget)
        self.lwFaces.setObjectName("lwFaces")
        self.horizontalLayout.addWidget(self.lwFaces)
        self.swFaces = QtWidgets.QStackedWidget(self.centralwidget)
        self.swFaces.setObjectName("swFaces")
        self.page = QtWidgets.QWidget()
        self.page.setObjectName("page")
        self.swFaces.addWidget(self.page)
        self.page_2 = QtWidgets.QWidget()
        self.page_2.setObjectName("page_2")
        self.swFaces.addWidget(self.page_2)
        self.horizontalLayout.addWidget(self.swFaces)
        self.horizontalLayout.setStretch(0, 1)
        self.horizontalLayout.setStretch(1, 7)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 28))
        self.menubar.setObjectName("menubar")
        self.mnFile = QtWidgets.QMenu(self.menubar)
        self.mnFile.setObjectName("mnFile")
        self.mnOptions = QtWidgets.QMenu(self.menubar)
        self.mnOptions.setObjectName("mnOptions")
        self.mnHelp = QtWidgets.QMenu(self.menubar)
        self.mnHelp.setObjectName("mnHelp")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.actExit = QtWidgets.QAction(MainWindow)
        self.actExit.setObjectName("actExit")
        self.actSetting = QtWidgets.QAction(MainWindow)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/icons/settings.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actSetting.setIcon(icon)
        self.actSetting.setObjectName("actSetting")
        self.actAbout = QtWidgets.QAction(MainWindow)
        self.actAbout.setObjectName("actAbout")
        self.mnFile.addAction(self.actExit)
        self.mnOptions.addAction(self.actSetting)
        self.mnHelp.addAction(self.actAbout)
        self.menubar.addAction(self.mnFile.menuAction())
        self.menubar.addAction(self.mnOptions.menuAction())
        self.menubar.addAction(self.mnHelp.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.mnFile.setTitle(_translate("MainWindow", "文件"))
        self.mnOptions.setTitle(_translate("MainWindow", "选项"))
        self.mnHelp.setTitle(_translate("MainWindow", "帮助"))
        self.actExit.setText(_translate("MainWindow", "退出"))
        self.actSetting.setText(_translate("MainWindow", "设置"))
        self.actAbout.setText(_translate("MainWindow", "关于"))

import res_rc
