# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'toFuleRecords.ui'
#
# Created: Fri Jan 22 09:03:18 2016
#      by: PyQt5 UI code generator 5.2.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_ToFuelRecordForm(object):
    def setupUi(self, ToFuelRecordForm):
        ToFuelRecordForm.setObjectName("ToFuelRecordForm")
        ToFuelRecordForm.resize(564, 437)
        self.verticalLayout = QtWidgets.QVBoxLayout(ToFuelRecordForm)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label = QtWidgets.QLabel(ToFuelRecordForm)
        self.label.setObjectName("label")
        self.horizontalLayout.addWidget(self.label)
        self.cmbVehicleNumber = QtWidgets.QComboBox(ToFuelRecordForm)
        self.cmbVehicleNumber.setObjectName("cmbVehicleNumber")
        self.horizontalLayout.addWidget(self.cmbVehicleNumber)
        self.label_2 = QtWidgets.QLabel(ToFuelRecordForm)
        self.label_2.setObjectName("label_2")
        self.horizontalLayout.addWidget(self.label_2)
        self.cmbYear = QtWidgets.QComboBox(ToFuelRecordForm)
        self.cmbYear.setObjectName("cmbYear")
        self.horizontalLayout.addWidget(self.cmbYear)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.horizontalLayout.addLayout(self.horizontalLayout_2)
        self.btnAdd = QtWidgets.QPushButton(ToFuelRecordForm)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/icons/add.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btnAdd.setIcon(icon)
        self.btnAdd.setObjectName("btnAdd")
        self.horizontalLayout.addWidget(self.btnAdd)
        self.btnDel = QtWidgets.QPushButton(ToFuelRecordForm)
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(":/icons/del.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btnDel.setIcon(icon1)
        self.btnDel.setObjectName("btnDel")
        self.horizontalLayout.addWidget(self.btnDel)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.tvRecords = QtWidgets.QTableView(ToFuelRecordForm)
        self.tvRecords.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.tvRecords.setObjectName("tvRecords")
        self.tvRecords.horizontalHeader().setStretchLastSection(True)
        self.verticalLayout.addWidget(self.tvRecords)
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_4.addItem(spacerItem)
        self.btnOk = QtWidgets.QPushButton(ToFuelRecordForm)
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap(":/icons/btn_ok.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btnOk.setIcon(icon2)
        self.btnOk.setObjectName("btnOk")
        self.horizontalLayout_4.addWidget(self.btnOk)
        self.btnCancel = QtWidgets.QPushButton(ToFuelRecordForm)
        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap(":/icons/btn_close.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btnCancel.setIcon(icon3)
        self.btnCancel.setObjectName("btnCancel")
        self.horizontalLayout_4.addWidget(self.btnCancel)
        self.verticalLayout.addLayout(self.horizontalLayout_4)

        self.retranslateUi(ToFuelRecordForm)
        QtCore.QMetaObject.connectSlotsByName(ToFuelRecordForm)

    def retranslateUi(self, ToFuelRecordForm):
        _translate = QtCore.QCoreApplication.translate
        ToFuelRecordForm.setWindowTitle(_translate("ToFuelRecordForm", "Form"))
        self.label.setText(_translate("ToFuelRecordForm", "车牌号"))
        self.label_2.setText(_translate("ToFuelRecordForm", "所在年份"))
        self.btnAdd.setText(_translate("ToFuelRecordForm", "添加"))
        self.btnDel.setText(_translate("ToFuelRecordForm", "移除"))
        self.btnOk.setText(_translate("ToFuelRecordForm", "确定"))
        self.btnCancel.setText(_translate("ToFuelRecordForm", "取消"))

import res_rc
